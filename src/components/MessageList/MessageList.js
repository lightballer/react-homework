import React from 'react';
import './MessageList.css';
import Message from '../Message/Message';
import Divider from '../Divider/Divider';
import OwnMessage from '../OwnMessage/OwnMessage';
import { getTextDateFromISO } from '../../helper';

class MessageList extends React.Component {

  isDividerRequired(prevDate, curDate) {
    prevDate = prevDate.split('').splice(0, 10).join('');
    curDate = curDate.split('').splice(0, 10).join('');
    if (prevDate === curDate) {
      return false;
    }
    return true;
  }
  
  render() {
    const { items, deleteMessage, updateMessageText } = this.props;
    let prevDate = '';
    return (
      <div className="message-list">
        {
          items.map(item => {
            if (item.user) {
              if (this.isDividerRequired(prevDate, item.createdAt)) {
                prevDate = item.createdAt;
                return (
                  <div>
                    <Divider date={ getTextDateFromISO(prevDate) }/>
                    <Message item={ item }/>
                  </div>
                );
              } else {
                return <Message item={ item }/>
              }
              
            } else {
              if (this.isDividerRequired(prevDate, item.createdAt)) {
                prevDate = item.createdAt;
                return (
                  <div >
                    <Divider date={ getTextDateFromISO(prevDate) }/>
                    <OwnMessage item={ item } deleteMessage={ deleteMessage } updateMessageText={ updateMessageText }/>
                  </div>);
              } else {
                return <OwnMessage item={ item }  deleteMessage={ deleteMessage } updateMessageText={ updateMessageText }/>
              }
            }
          })
        }
      </div> 
    );
  }
}
  
export default MessageList;
