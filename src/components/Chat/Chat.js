import React from 'react';
import './Chat.css';
import Header from '../Header/Header';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import Preloader from '../Preloader/Preloader';

class Chat extends React.Component {

  USER_ID = 'some-random-id';

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }
  
  componentDidMount() {
    fetch(this.props.url)
      .then(response => response.json())
      .then(
        data => {
          this.setState({
            isLoaded: true,
            items: data
          });
        }, 
        error => {
          this.setState({
             error,
             isLoaded: true
          });
      });
  }

  createMessage(text, createdAt) {
    const newMessage = {
      id: createdAt,
      userId: this.USER_ID,
      text,
      createdAt,
    };
    this.setState({
      items: [...this.state.items, newMessage],
    });
  }

  deleteMessage(id) {
    const prevItems = [...this.state.items];
    const newItems = prevItems.filter(item => item.id !== id);
    this.setState({
      items: newItems,
    });
  }

  updateMessageText(id, newText) {
    const prevItems = [...this.state.items];
    const newItems = prevItems.map(item => {
      if (item.id === id) {
        item.text = newText;
      }
      return item;
    });
    this.setState({
      items: newItems,
    });
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <p>Error to fetch!</p>
    } else if (!isLoaded) {
      return <Preloader/>;
    } else {
      const participantsCount = items.reduce((acc, cur) => {
        if (!acc.includes(cur.userId)) 
          acc.push(cur.userId);
          return acc;
        }, []).length;
      return (
        <div className="App">
          <Header 
            participantsCount={ participantsCount } 
            messagesCount={ items.length } 
            lastMessageTime={ items[items.length - 1].createdAt } />
          <MessageList 
            items={ items } 
            deleteMessage={ this.deleteMessage.bind(this) } 
            updateMessageText={ this.updateMessageText.bind(this) }/>
          <MessageInput createMessage={ this.createMessage.bind(this) }/>
        </div>
      ); 
    }
  }
}

export default Chat;
