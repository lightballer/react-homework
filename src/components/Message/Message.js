import React from 'react';
import './Message.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import { getTimeFromISO } from '../../helper';

class Message extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isLiked: false,
    };
  }

  onLikeClick() {
    this.setState({
      isLiked: !this.state.isLiked,
    });
  }

  render() {
    const { avatar, user, text, createdAt } = this.props.item;
    return (
      <div className="message">
        <img className="message-user-avatar" src={ avatar } alt="avatar"/>    
        <div className="message-body">
          <div className="message-user-name">{ user }</div>
          <div className="message-text">{ text }</div>
        </div>
        <div className="message-metadata">
          <span className="message-time">{ getTimeFromISO(createdAt) }</span>
          <div className={ this.state.isLiked ? 'message-liked' : 'message-like' } onClick={ this.onLikeClick.bind(this) }><FontAwesomeIcon icon={ faHeart } /></div>
        </div>
      </div>
    );
  }
}

export default Message;
