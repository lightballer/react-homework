import React from 'react';
import './MessageInput.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowAltCircleUp } from "@fortawesome/free-solid-svg-icons";


class MessageInput extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  setInputValue(event, keyword) {
    const value = event.target.value;
    this.setState({
      [keyword]: value,
    })
  }

  onSendMessage() {
    const createdAt = new Date().toISOString();
    this.props.createMessage(this.state.text, createdAt);
    this.setState({
      text: '',
    })
  }

  onEnterPressed(event) {
    const key = event.key;
    if (key === 'Enter') {
      this.onSendMessage();
    }
  }

  render() {
    return (
      <div className="message-input">
        <input 
          type="text"
          value={this.state.text}
          className="message-input-text" 
          placeholder="Message" 
          onChange={ event => this.setInputValue(event, 'text')}
          onKeyPress= { this.onEnterPressed.bind(this) }/>
        <button 
          className="message-input-button" 
          onClick={ this.onSendMessage.bind(this) }>
            <span className="send-icon"><FontAwesomeIcon icon={ faArrowAltCircleUp }/></span> 
            Send</button>
      </div>
    );
  }
}
  
export default MessageInput;