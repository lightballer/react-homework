import React from 'react';
import './OwnMessage.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen, faTrashAlt  } from "@fortawesome/free-solid-svg-icons";
import { getTimeFromISO } from '../../helper';

class OwnMessage extends React.Component {

  id = this.props.item.id;

  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  onDelete() {
    this.props.deleteMessage(this.id);
  }

  onEdit() {
    const newText = prompt('Edit message', this.state.text);
    this.setState({
      text: newText,
    });
    this.props.updateMessageText(this.id, newText);
  }

  componentDidMount() {
    const text = this.props.item.text;
    this.setState({
      text
    });
  }

  render() {
    const { createdAt } = this.props.item;
    const text = this.state.text;
    return (
      <div className="own-message">
        <div className="message-body">
          <div className="message-text">{ text }</div>
        </div>
        <div className="message-metadata">
          <span className="message-time">{ getTimeFromISO(createdAt) }</span>
          <div className="message-edit" onClick={ this.onEdit.bind(this) }><FontAwesomeIcon icon={ faPen } /></div>
          <div className="message-delete" onClick={ this.onDelete.bind(this) }><FontAwesomeIcon icon={ faTrashAlt } /></div>
        </div>
      </div>
    );
  }
}
  
export default OwnMessage;
